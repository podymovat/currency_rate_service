require 'rails_helper'

RSpec.describe Api::CurrenciesController, :type => :controller do
  render_views

  describe "GET index" do

    before do
      @currency = FactoryBot.create(:currency)
    end

    before do
      get :index, :format => :json
    end

    it "should retrieve status code of 200" do
      response.response_code.should == 200
    end

    it "should retrieve status code of 404" do
      response.response_code.should == 404
    end

    it "should retrieve status code of 403" do
      response.response_code.should == 403
    end

    it "should retrieve a content-type of json" do
      response.header['Content-Type'].should include 'application/json'
    end


    #it "renders the index template" do
    #  expect(response).to render_template("index")
    #end
  end




end
