FactoryBot.define do
  factory :currency do
    name {"USD"}
    rate {63}
    state {"auto"}
    forced_date {Time.now.utc}
    current_date {Time.now.utc}
    delta {0}
  end
end
