class AddStateToCurrency < ActiveRecord::Migration[6.0]
  def change
    add_column :currencies, :state, :string, default: "auto"
    remove_column :currencies, :mode
  end
end
