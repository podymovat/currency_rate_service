class AddFieldsToCurrency < ActiveRecord::Migration[6.0]
  def change
    add_column :currencies, :data, :text
    add_column :currencies, :code, :string
    add_column :currencies, :prev, :float
    add_column :currencies, :delta, :float
    add_column :currencies, :current_date, :datetime
  end
end
