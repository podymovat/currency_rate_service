class CreateCurrencyHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :currency_histories do |t|
      t.integer :currency_id
      t.string :code
      t.float :rate
      t.text :data

      t.timestamps
    end
  end
end
