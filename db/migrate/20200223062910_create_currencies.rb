class CreateCurrencies < ActiveRecord::Migration[6.0]
  def change
    create_table :currencies do |t|
      t.string :name
      t.float :rate, default: 0.00
      t.datetime :forced_date
      t.string :mode, default: "auto"

      t.timestamps
    end
  end
end
