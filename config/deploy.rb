# config valid for current version and patch releases of Capistrano
lock "~> 3.12.0"

set :application, 'currency_rate_service'
set :repo_url, 'git@bitbucket.org:podymovat/currency_rate_service.git'
set :branch, :develop
set :deploy_to, '/home/deploy/currency_rate_service'
set :stage,           :production
set :deploy_via,      :remote_cache
set :pty, true
set :linked_files, %w{config/database.yml config/secrets.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads public/packs public/assets node_modules client/node_modules}
set :keep_releases, 5
set :rvm_type, :user
set :rvm_ruby_version, '2.6.3'
set :rvm_map_bins, fetch(:rvm_map_bins, []).push('rvmsudo')

set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.access.log"
set :puma_error_log,  "#{release_path}/log/puma.error.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true

set :copy_files, %w[node_modules]
set :assets_prefix, 'packs'
#set :subdir, "server"

set :nvm_type, :user
set :nvm_node, 'v10.19.0'
set :nvm_map_bins, %w{node npm yarn}

#set :yarn_target_path, -> { release_path.join('subdir') } # default not set
set :yarn_flags, '--production --silent --no-progress'    # default
set :yarn_roles, :all                                     # default
set :yarn_env_variables, {}                               # default


  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end
