const { environment } = require('@rails/webpacker')

var path = require('path');

const webpack = require('webpack')
environment.plugins.append('Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery/src/jquery',
    'windows.jQuery': 'jquery/src/jquery',
    Popper: ['popper.js', 'default'],
    datepicker: 'bootstrap-datepicker/js/bootstrap-datepicker.js'
  })
)

environment.resolve = {
  alias: {
    'jquery': path.join(__dirname, 'node_modules/jquery/src/jquery')
  }
};

module.exports = environment;
