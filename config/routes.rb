Rails.application.routes.draw do

  namespace :api, defaults: {format: 'json'} do
    resources :currencies
  end

  scope :module => :web do
    root to: 'welcome#index'


    namespace :admin do
      root to: 'currencies#index'

      resources :currencies

    end

  end

end
