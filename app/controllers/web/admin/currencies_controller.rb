class Web::Admin::CurrenciesController < Web::Admin::ApplicationController

  def index
    @currency = Currency.first
  end


  def update
    @currency = Currency.find(params[:id])
    if @currency.update_attributes(permit_params)
       @currency.manually

      flash[:success] = t("message.updated")

      redirect_to admin_root_path
    else
      flash[:alert] = @currency.errors.full_messages

      render :index
    end
  end


  private

  def permit_params
    params.require(:currency).permit(:name, :rate, :forced_date, :state, :updated_at)
  end



end
