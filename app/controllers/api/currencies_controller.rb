class Api::CurrenciesController < Api::ApplicationController

  def index
    @currency = Currency.first
  end


  private

  def permit_params
    params.require(:currency).permit(:name, :rate, :forced_date, :state, :updated_at)
  end



end
