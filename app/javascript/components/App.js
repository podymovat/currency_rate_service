import React, {Component} from 'react';
import {Provider} from 'react-redux';
import ReduxToastr from 'react-redux-toastr'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'

import {configureStore} from "../configureStore";
const store = configureStore();

import CurrencyComponent from "./Currency";

class App extends Component {
    render() {
        return (
            <div>
                <Provider store={store}>
                  <CurrencyComponent/>
                  <ReduxToastr
                      transitionIn="fadeIn"
                      transitionOut="fadeOut"
                      preventDuplicates
                      timeOut={99999999}
                  />
                </Provider>
            </div>
        );
    }
}

export default App;
