import * as React from 'react';
import {connect} from 'react-redux';
import {loadCurrency} from "../currency_actions";
import {withPolling} from "../withPolling";

class Currency extends React.Component {
  render () {
    return (
      <React.Fragment>
        <h3>Курс валют ЦБ РФ на {this.props.currency.currency_date}</h3>
        <br/>
        <p>
        1{this.props.currency.name} - {this.props.currency.rate} ({this.props.currency.delta})RUB
        </p>
        <small>Последнее обновление базы данных: {this.props.currency.updated_date}</small>
      </React.Fragment>
    );
  }

//  componentDidMount() {
//    fetch('http://localhost:3000/api/currencies')
//      .then(response => response.json())
//      .then(data => this.setState({ currency: data }));
//  }
}

const mapStateToProps = state => ({
    currency: state.data.currency
});
const mapDispatchToProps = {};
export default withPolling(loadCurrency)(
    connect(mapStateToProps, mapDispatchToProps)(Currency));
