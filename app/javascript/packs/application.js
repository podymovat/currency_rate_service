require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require("jquery")


import 'bootstrap'
import 'bootstrap-datepicker'


document.addEventListener("turbolinks:load", () => {
  $('[data-toggle="tooltip"]').tooltip()
})


$('.datepicker').datepicker();
// Support component names relative to this directory:
var componentRequireContext = require.context("components", true);
var ReactRailsUJS = require("react_ujs");
ReactRailsUJS.useContext(componentRequireContext);
