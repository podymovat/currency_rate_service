import {LOAD_DATA_SUCCESS} from "./currency_actions";
const initialState = {
    currency: []
};
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case LOAD_DATA_SUCCESS: {
            return {
                ...state,
                currency: action.data
            }
        }
        default: {
            return state;
        }
    }
}
