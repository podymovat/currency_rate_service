class Currency < ApplicationRecord
  has_many :currency_histories

  serialize :data

  #validates :name, presence: true, uniqueness: true
  validates :rate, presence: true, numericality: {:greater_than_or_equal_to => 0}
  #validates :forced_date, presence: true
  #validate :forced_date_after_now


  state_machine :state, :initial => :auto do
    state :auto
    state :manual

    event :automatically do
      transition all => :auto
    end

    event :manually do
      transition all => :manual
    end
  end


  def to_s
    name
  end



  private

  def forced_date_after_now
    return if forced_date.blank?

    if forced_date < Time.now.utc
      errors.add(:forced_date, I18n.t('activerecord.errors.models.currency.attributes.forced_date.in_past'))
    end
 end

end
