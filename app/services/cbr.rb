module Cbr
  class << self
    URL = "https://www.cbr-xml-daily.ru/daily_json.js"
    CURRENT_CURRENCY = "USD"

    def load
      obj = json_parse get_request.body
      data = build_params(obj)

      currency = Currency.find_by_name(CURRENT_CURRENCY)
      if currency.update(data)
        currency.automatically
        currency.currency_histories.build(code: data['code'], rate: data['rate'], data: data['data'])
        currency.save!
      else
      end
    end


    private

    def get_request
      Rails.logger.info('Fetching url: %s' % URL)
      begin
        request = RestClient.get URL
      rescue RestClient::ExceptionWithResponse => error
        Rails.logger.info('Error: %s Url: %s' % [error,URL])
      end
      request
    end

    def build_params obj
      c = obj["Valute"]["USD"]
      {
        name: CURRENT_CURRENCY,
        rate: c["Value"],
        code: c["ID"],
        prev: c["Previous"],
        delta: c["Value"] - c["Previous"],
        current_date: obj["Date"],
        data: c
      }
    end

    def json_parse json
      return {} unless json

      begin
        result = JSON.parse(json)
        result
      rescue
        nil
      end

    end



  end
end
