module ApplicationHelper

  def flash_html_safe str
		return if str.nil?

		Sanitize.fragment(str, Sanitize::Config::BASIC).html_safe
	end


  def flash_class level
      case level
      when 'notice' then "alert alert-primary"
          when 'success' then "alert alert-success"
          when 'warning' then "alert alert-warning"
          when 'error' then "alert alert-danger"
          when 'alert' then "alert alert-danger"
      end
  end

end
