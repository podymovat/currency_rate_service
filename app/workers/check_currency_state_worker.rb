class CheckCurrencyStateWorker
  include Sidekiq::Worker

  def perform
    currency = Currency.first
    if Time.now.utc >= currency.forced_date
      currency.automatically!
      puts "Set auto"
    end
  end
end
