json.name @currency.name
json.rate @currency.rate.round(2)
json.currency_date @currency.current_date.strftime("%Y-%d-%m")
json.updated_date @currency.updated_at.strftime("%Y-%d-%m %H:%m")


json.delta (@currency.delta) > 0 ? "+#{@currency.delta.round(2)}" : "-#{@currency.delta.round(2)}"
